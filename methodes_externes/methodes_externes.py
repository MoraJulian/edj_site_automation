import pandas as pd

def extraire_noms_eleves_profs(path_excel):
    df = pd.ExcelFile(path_excel).parse()
    colomns_des_profs = df.filter(like='Professeur').columns
    if len(colomns_des_profs) != 0:
        noms_des_profs = df[colomns_des_profs[0]].drop_duplicates().dropna()
        print(noms_des_profs)
        return ['professeurs', noms_des_profs]
    else: 
        noms_des_eleves = df['Nom complet'].drop_duplicates().dropna()
        return ['eleves',noms_des_eleves]


