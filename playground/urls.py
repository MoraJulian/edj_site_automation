from pydoc import visiblename
from unicodedata import name
from django.urls import path
from . import views

urlpatterns = [
    path('page-televersement/', views.page_televersement, name ='page-televersement'),
    path('efface_destination/<nom>', views.efface_destination, name='efface_destination'),
    path('televerser_documents', views.televerser_documents, name='televerser_documents' ),
    path('', views.acceuil, name='acceuil'),
    path('page-supprimer/', views.page_supprimer, name='page-supprimer'),
    path('supprimer_fiches', views.supprimer_fiches, name='supprimer_fiches'),
    path('page-renommer/', views.page_renommer, name='page-renommer'),
    path('renommer_fiches', views.renommer_fiches,name='renommer_fiches' )
]