from multiprocessing import context
from django.http import HttpResponse
from django.shortcuts import render
import methodes_externes.methodes_dropbox as md
import methodes_externes.methodes_externes as me

# Create your views here.
def acceuil(request):
    return render(request, 'acceuil.html')

def page_supprimer(request):
    context = {}
    if 'liste_des_fiches' in request.session:
        context['liste_des_fiches'] = request.session['liste_des_fiches']
    else:
        context['liste_des_fiches'] = []

    if "GET" == request.method:
        return render(request, 'page-supprimer.html', context)
    else:
        nom_du_fiche = request.FILES["fiche"]
        context['liste_des_fiches'] = md.return_liste_efface(nom_du_fiche.name)
        request.session['liste_des_fiches'] = context['liste_des_fiches']
        return render(request, 'page-supprimer.html', context)

def supprimer_fiches(request):
    context = {}
    liste_des_fiches = request.session['liste_des_fiches']
    md.efface_fiches(liste_des_fiches)
    return render(request, 'page-supprimer.html', context)


def page_televersement(request):
    context = {}
    context['fiches_a_deplacer'] = md.fiches_a_copier_dans_dropbox()
    request.session.flush()

    if "GET" == request.method:
        return render(request, 'page-televersement.html', context)
    else:
        fiche_excel = request.FILES["excel_file"]
        type_de_fiche_et_contenu =  me.extraire_noms_eleves_profs(fiche_excel)

        request.session['type'] = type_de_fiche_et_contenu[0]
        context["liste_des_destinations"] = type_de_fiche_et_contenu[1]

        for destination in context['liste_des_destinations']:
            if not 'liste_des_destinations' in request.session:
                request.session['liste_des_destinations'] = [destination]
            else:
                temp_list = request.session['liste_des_destinations']
                temp_list.append(destination)
                request.session['liste_des_destinations'] = temp_list
        
        
        return render(request, 'page-televersement.html', context)

def page_renommer(request):
    context = {}

    if "GET" == request.method:
        return render(request, 'page-renommer.html', context)
    else:
        fichier = request.FILES["file"]
        request.session['nom_de_la_fiche'] = fichier.name
        request.session['nom_renommer'] = request.POST.get('nom_renommer')
        context['liste_des_paths'] = md.return_liste_efface(fichier.name)
        request.session['liste_des_paths'] = context['liste_des_paths']
        return render(request, 'page-renommer.html', context)

def renommer_fiches(request):
    context = {}
    nom_de_la_fiche = request.session['nom_de_la_fiche']
    nom_renommer = request.session['nom_renommer']
    md.renommer_fiches(nom_de_la_fiche, nom_renommer)
    return render(request,'page-renommer.html', context)


def efface_destination(request, nom):
    context = {}
    context['fiches_a_deplacer'] = md.fiches_a_copier_dans_dropbox()

    liste_actuelle = request.session.get('liste_des_destinations')
    liste_actuelle.remove(nom)

    request.session['liste_des_destinations'] = liste_actuelle
    context['liste_des_destinations'] = liste_actuelle
    return render(request, 'page-televersement.html', context)

def televerser_documents(request):
    context = {}
    context['fiches_a_deplacer'] = md.fiches_a_copier_dans_dropbox()
    liste_des_destinations = request.session.get('liste_des_destinations')
    if request.session.get('type') == 'professeurs':
        md.copier_fiches_profs(liste_des_destinations)
    else:
        md.copier_fiches(liste_des_destinations)
    
    context['reussi'] = "Téléversement Reussi"

    return render(request, 'page-televersement.html', context)
